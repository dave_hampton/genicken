/* jslint undef: true, evil: true, browser: true, unparam: true, sloppy: true, white: true, nomen: true, regexp: true, maxerr: 50, plusplus: true, indent: 4 */
/* global jQuery:true */
/*
 * Copyright 2015  Dave Hampton  All rights reserved
 */
var fs = require('fs'),
  path = require('path'),
  util = require('util'),
  grep = require('grep1'),
  _ = require('underscore'),
  deferred = require('jquery-deferred'),
  isHiddenFile=/^\./,
  express = require('express'),
  jade = require('jade'),
	router = express.Router(),
	nodemailer = require("nodemailer"),
	mg = require('nodemailer-mailgun-transport'),
	title = "Genicken ",
	//Mailgun Auth
	auth = {
		auth: {
			api_key: 'key-474c856eeb7d552244e03f3ffd182111',
			domain: 'mail.genicken.com'
		}
	},
	/* GET home page. */
	transporter = nodemailer.createTransport(mg(auth)),

	//reCaptcha keys
	SITE_KEY = '6Ld8DwkTAAAAAMvispfXogJS_B_kozHk0AfL7glG',
	SECRET_KEY = '6Ld8DwkTAAAAAECcLFNlVrzxsHj_dVZ7b_wxb571',
	recaptcha = require('express-recaptcha'),
  getReqURL = function (req) {
    return req.protocol + '://' + req.get('host') + req.originalUrl;
  },
  posts, flatPosts=[],
  postsDeferred = deferred.Deferred(),
  removeHiddenFiles = function(flatPosts) {
    return _.filter(flatPosts, function (post) {
      return !isHiddenFile.test(post.name)
    });
  },


// get Blog Posts in a json structure
  dirTree = function (filename, flat) {
    var stats = fs.lstatSync(filename),
        url = filename.split('/').slice(2).join('/'),
        info, dateNum, dateString, deferredGrep = deferred.Deferred(),
        deferredDirTree = deferred.Deferred(),
        childDeferreds=[], children;
    url = url.substr(0, url.indexOf('.jade'));

    info = {
        path: filename,
        name: path.basename(filename, '.jade'),
        children: []
    };

    if (stats.isDirectory()) {
        fs.readdirSync(filename).map(function(child) {
          childDeferredDirTree = dirTree(filename + '/' + child, flat);
          childDeferreds.push(childDeferredDirTree);
          childDeferredDirTree.done(function (results) {
            info.children.push(results);
            info.children = removeHiddenFiles(info.children);
          });
        });
        children = deferred.when.apply(deferred, childDeferreds);
        children.done(function (results) {
          deferredGrep.resolve(info);
        });
    } 
    else {
      info.url = encodeURI(url);
      grep(['postDateValue', info.path] , function (err, stdout, stderr) {
        if (err || stderr) {
          //console.log(err, stderr);
        } else {
          console.log(stdout)
          info.postDate = new Date(stdout.split('\'')[1]);
        }
        if (!_.isUndefined(flat) && !isHiddenFile.test(info.name)) {
          flat.push(info);
        }
        deferredGrep.resolve(info);
      });
    }
    deferredGrep.done(function(results) {
      deferredDirTree.resolve(results);
    });
    return deferredDirTree.promise();    
  },

  sortReverse = function (posts) {
    return _.sortBy(posts, function(post) {
        return post.name * -1;
    });
  };

recaptcha.init(SITE_KEY, SECRET_KEY);

postsDeferred = dirTree('./views/posts', flatPosts);
postsDeferred.done( function (results) {
  posts = results;
  _.each(flatPosts, function (post, index, list) {
    post.previous = {};
    post.next = {};
    if (index > 0) {
      post.previous = {
        name: list[index - 1].name,
        url: list[index - 1].url
      }
    }
    if (index < list.length - 1) {
      post.next = {
        name: list[index + 1].name,
        url: list[index + 1].url
      }
    }

  });
  posts = posts.children;
  posts = sortReverse(posts);
  _.each(posts, function (post) {
    post.children = sortReverse(post.children);
  });
    
  console.log(util.inspect(posts, false, null));
  //Puts the list in rough order - will need to sort by postdate...
  flatPosts = _.sortBy(flatPosts, 'postDate');
  flatPosts.reverse();
  console.log('FlatPosts: ');
  console.log(util.inspect(flatPosts, false, null));
});
postsDeferred.done(function () {
  router.get('/privacy', function (req, res, next) {
    res.render('privacy.jade', {
      reqURL: getReqURL(req),
      title: title + '- Terms, Conditions & Privacy',
      lead: "/"
    });
  });

  router.get('/portfolio/:topic', function (req, res, next) {
    res.render('portfolio/' + req.params.topic + '.jade', {
      reqURL: getReqURL(req),
      title: title + ": " + req.params.topic,
      lead: "/"
    });
  });


  router.get('/', recaptcha.middleware.render, function (req, res, next) {
    res.render('index', {
      reqURL: getReqURL(req),
      title: title,
      captcha: req.recaptcha,
      lead: ''
    });
  });

  router.get('/blog/:start?', function (req, res, next) {
    var start = req.params.start || 0;
    res.render('blog.jade', {
      title: title + ' - Genicken Internal Blog',
      lead: "/",
      reqURL: getReqURL(req),
      posts: posts,
      previous: start - 4 <= 0 ? null : start - 4,
      next: start + 4 < flatPosts.length ? start + 4 : null,
      flatPosts: _.chain(flatPosts).rest(start).first(4).value(),
      templateRender: jade.renderFile
    });
  });

  router.get('/posts/:year/:month/:title', function (req, res, next) {
    var post = ['posts', req.params.year, req.params.month, req.params.title].join('/'),
    
    meta =  _.findWhere(flatPosts, {url: encodeURI(post)});

    res.render(post + '.jade', {
      title: title + ' - ' + decodeURIComponent(req.params.title),
      lead: "/",
      reqURL: getReqURL(req),
      pageTitle: decodeURIComponent(req.params.title),
      posts: posts,
      postMeta: meta
    });
  });

  /*router.get('/', function(req, res, next) {
    res.render('index', { title: title });
  });*/


  router.post('/contact', recaptcha.middleware.verify, function (req, res, next) {
    var msg = ('Name: ' + req.body.name + '\nPhone: ' + req.body.phone + '\nEmail: ' + req.body.email + '\nMessage: ' + req.body.message),
      mailOptions = {
        from: 'support@genicken.com',
        to: 'jacksonfriggle@gmail.com',
        subject: 'GENICKEN CONTACT FORM MESSAGE',
        text: msg
      },
      postOptions = {
        reqURL: getReqURL(req),
        title: title,
      };

    if (!req.recaptcha.error) {
      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          res.render('includes/msgFailed.jade', postOptions);
        } else {
          res.render('includes/msgSent.jade', postOptions);
        }
      });
    } else {
      console.log('recaptcha failed');
      console.log(req.recaptcha.error);
      res.render('includes/msgFailed.jade', postOptions);
    }
  });
  router.get('/feeds/:blogType/rss', function(req, res, next) {
    var mappedFeed = {
      memos: {
        title: "Classified Internal Memos",
        description: "We have Hacked YouR $ite! W3 are MonoGaMous!",
        blogLoc: "http://www.genicken.com/blog",
        posts: _.first(flatPosts, 52)
      }    
    },
    shared = {
      reqURL: getReqURL(req)
    },
    merged = _.extend(shared, mappedFeed[req.params.blogType]);
  
    res.render('feed', merged);
  });
});


module.exports = router;
